# CSS Animation

#### 介绍

This is a repository for CSS Animation practice.
These are all fun and nice little practices.


#### 使用说明

1.  All practices are from the Internet, and there are links to the original article inside.
2.  Just code these to learn, not for commercial purposes.
3.  If you are insterested in them, you can do for fun, never for commercial purposes.
    If you want to publish remember to add a link to the original article.  
4.  You can create a Pull Request to share with me some funny CSS Animations.

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


